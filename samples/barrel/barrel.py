import os
import sys
import json
import datetime
import numpy as np
import skimage.draw
import imgaug
from cv2 import cv2

from random import randint

confidence_border = 0.9995
class Logger(object):
    def __init__(self, path):
        self.terminal = sys.stdout
        self.log = open(path, 'a')

    def __del__(self):
        sys.stdout = self.terminal
        self.log.close()

    def write(self, msg):
        self.terminal.write(msg)
        self.log.write(msg)

    def flush(self):
        self.log.flush()
        self.terminal.flush()

# Ignore all warnings
# TODO: remove warnings only from skimage
import warnings
warnings.filterwarnings("ignore")

# Root directory of the project
ROOT_DIR = os.path.abspath("../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils

# Path to trained weights file
COCO_WEIGHTS_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")

# Directory to save logs and model checkpoints, if not provided
# through the command line argument --logs
DEFAULT_LOGS_DIR = os.path.join(ROOT_DIR, "logs")

############################################################
#  Configurations
############################################################

class BarrelConfig(Config):
    """Configuration for training on the barrel dataset.
    Derives from the base Config class and overrides some values.
    """
	
   
    # Give the configuration a recognizable name

    NAME = "barrel"

    # Adjust down if you use a smaller GPU.
    IMAGES_PER_GPU = 1
    GPU_COUNT = 1

    # Number of classes (including background)
    NUM_CLASSES = 1 + 1 + 1 # Background + standing + not

    # No resizing, just padding
    IMAGE_RESIZE_MODE = "square"
    IMAGE_MIN_DIM = 640
    IMAGE_MAX_DIM = 640

    # Number of training steps per epoch
    STEPS_PER_EPOCH = 100

    # Skip detections with < 90% confidence
    DETECTION_MIN_CONFIDENCE = confidence_border

    USE_MINI_MASK = False

class BarrelDataset(utils.Dataset):

    def load_barrel(self, dataset_dir, subset):
        """Load a subset of the Barrel dataset.
        dataset_dir: Root directory of the dataset.
        subset: Subset to load: train or val
        """
        # Add classes. We have two classes to add.
        self.add_class("barrel", 1, "standing")
        self.add_class("barrel", 2, "lying")

        # Train or validation dataset?
        assert subset in ["train", "val"]
        json_dir = os.path.join(dataset_dir, subset) + '/'

        	# Load annotations
        # Palych saves each image in the form:
        # { 'filename': '28503151_5b5b7ec140_b.jpg',
        #        '0': {
        #               'x': [...],
        #               'y': [...],
        #       ... more barrels ...
        #   },
        # }
            

        for filename in os.listdir(json_dir):

            newJson = json.load(open(json_dir + filename))

            image_path = os.path.join(dataset_dir, "pics" , newJson['filename'])
            height = 480
            width = 640 

            mask = np.zeros((height, width, newJson['amount']), dtype=np.uint8)
            
            class_ids = []

            for i in range(newJson['amount']):
                barrelType = newJson['barrels'][str(i)]["type"]
                if barrelType == 0:
                    barrelType = 1
                elif barrelType == 1:
                    barrelType = 2
                else:
                    raise Exception("barrel type must be either standing (0) or lying (1)")
                
                class_ids.append(barrelType)

                for j in range(len(newJson["barrels"][str(i)]["y"])):
                    x = newJson["barrels"][str(i)]["y"][j]
                    y = newJson["barrels"][str(i)]["x"][j]
                    mask[x][y][i] = 1

            assert mask.shape[-1] == len(class_ids)
            self.add_image(
                "barrel",
                image_id=newJson['filename'],  # use file name as a unique image id
                path=image_path,
                mask=mask, 
                class_ids=np.array(class_ids))

    def load_mask(self, image_id):
        """Generate instance masks for an image.
       Returns:
        masks: A bool array of shape [height, width, instance count] with
            one mask per instance.
        class_ids: a 1D array of class IDs of the instance masks.
        """
        # If not a balloon dataset image, delegate to parent class.
        image_info = self.image_info[image_id]
        if image_info["source"] != "barrel":
            return super(self.__class__, self).load_mask(image_id)

        mask = image_info["mask"]
        class_ids = image_info["class_ids"]

        return mask.astype(np.bool), class_ids

    def image_reference(self, image_id):
        """Return the path of the image."""
        info = self.image_info[image_id]
        if info["source"] == "balloon":
            return info["path"]
        else:
            super(self.__class__, self).image_reference(image_id)


'''
run evaluation on an image or a video
'''
def eval_model(model, image_path=None, video_path=None, videoCapture=None, folder=None, showimg=True, verbose=False, runConstantly=False):
    assert image_path or video_path or videoCapture or folder

    # Image or video?
    if image_path:
        # Run model detection
        print("Running on {}".format(args.image))
        # Read image
        image = cv2.imread(image_path)
        # Detect objects
        objs = model.detect([image], verbose=0)[0]
        # Save output
        image = add_detected(image, objs['masks'])
        if showimg:
            cv2.imshow("Detected objects", image)
            cv2.waitKey(0)
        else:
            cv2.imwrite("detected" + image_path, image)
    elif video_path:
        # Video capture
        vcapture = cv2.VideoCapture(video_path)
        width = int(vcapture.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(vcapture.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fps = vcapture.get(cv2.CAP_PROP_FPS)

        # Define codec and create video writer
        file_name = "eval_{:%Y%m%dT%H%M%S}.avi".format(datetime.datetime.now())
        vwriter = cv2.VideoWriter(file_name,
                                  cv2.VideoWriter_fourcc(*'MJPG'),
                                  fps, (width, height))

        count = 0
        success = True
        while success:
            print("frame: ", count)
            # Read next image
            success, image = vcapture.read()
            if success:
                # OpenCV returns images as BGR, convert to RGB
                image = image[..., ::-1]
                # Detect objects
                r = model.detect([image], verbose=0)[0]
                # Add detected barrels to the frame
                image = add_detected(image, r['masks'])
                # Add image to video writer
                vwriter.write(image)
                count += 1
        vwriter.release()
        print("Saved to ", file_name)
    elif videoCapture:
        while True:
            # Read frame from capture
            a, img = videoCapture.read()
            # Detect objects
            r = model.detect([img[..., ::-1]], verbose=0)[0]
            # Add detected barrels to the frame
            img = add_detected(img, r['masks'])
            cv2.imshow("Detected barrels", img)
            if cv2.waitKey(1) == 27:
                break
    elif folder:
        if verbose:
            print('Im verbose')
        done = []
        from time import sleep, time
        while True:
            prevtime = time()
            filenames = [a for a in os.listdir(folder) if a.endswith(".png") and a not in done]
            done+=filenames
            if len(filenames) == 0 and not runConstantly:
                return
            count = 0
            #print('reading filenames: ' + str(time() - prevtime))
            for f in filenames:
                sleep(0.03)
                print('slept: 0.03s')
                prevtime = time()
                img = None
                imgpath = os.path.join(folder, f)
                while not (type(img) == type(np.array(['ya', 'ebal', 'python']))):
                    img = cv2.imread(imgpath)
                print('reading image: ' + str(time() - prevtime))
                prevtime = time()
                r = model.detect([img[..., ::-1]], verbose=0)[0]
                print('predicting: ' + str(time() - prevtime))
                prevtime = time()
                #img = add_detected(img, r['masks'])
                if verbose:
                    img = showbboxes(img, r['rois'])
                    img = showScore(img, r['scores'], r['rois'], r['class_ids'])
                if showimg:
                    cv2.imshow("Detected barrels", img)
                    cv2.waitKey(0)
                elif runConstantly:
                    data = {}
                    data['filename'] = f
                    data['amount']=len(r['scores'])
                    for i in range(len(r['scores'])):
                        data['barrels'] = {}
                        for i in range(r['masks'].shape[-1]):
                            data['barrels'][str(i)] = {}
                            coords = np.nonzero(r['masks'][:,:,i])
                            data['barrels'][str(i)]['type'] = int(r['class_ids'][i])-1
                            data['barrels'][str(i)]['x'] = coords[1].tolist()
                            data['barrels'][str(i)]['y'] = coords[0].tolist()
                    with open(os.path.join(folder, f[:-4] + ".json"), 'w') as outfile:
                        json.dump(data, outfile)
                    print('saving: ' + str(time() - prevtime))
                else:
                    cv2.imwrite("../detected/neuro/" + f, img)
                    count += 1
                    print("Wrote img ", count, " out of ", len(filenames))
            sleep(0.03)

def eval_embed():
    class InferenceConfig(BarrelConfig):
            # Set batch size to 1 since we'll be running inference on
            # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
            GPU_COUNT = 1
            IMAGES_PER_GPU = 1
            DETECTION_MIN_CONFIDENCE = confidence_border

            IMAGE_MIN_DIM = 800
            IMAGE_MAX_DIM = 1024
    model = modellib.MaskRCNN(mode="inference", config=InferenceConfig(),
                                  model_dir="logss")
    model.load_weights("mask_rcnn_barrel_0260.h5", by_name=True, exclude=[
            "mrcnn_class_logits", "mrcnn_bbox_fc",
            "mrcnn_bbox", "mrcnn_mask"])

    img = cv2.imread("autism.png")

    return model.detect([img[..., ::-1]], verbose=0)[0]['masks']

def showbboxes(img, res):
    for i in range(len(res)):
        cv2.rectangle(img, (res[i][1], res[i][0]), (res[i][3], res[i][2]), (randint(0, 255), randint(0, 255), randint(0, 255)), thickness=3)
    return img

def showScore(img, res, coords, classes):
    for i in range(len(res)):
        cv2.putText(img, str(res[i]), (coords[i][3], coords[i][2]), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX, 0.5, (randint(0, 255), randint(0, 255), randint(0, 255)))
        cv2.putText(img, str(i), (coords[i][3], coords[i][2]+10), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX, 0.5, (randint(0, 255), randint(0, 255), randint(0, 255)))
        cv2.putText(img, str(classes[i]), (coords[i][3]+10, coords[i][2]+10), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX, 0.5, (randint(0, 255), randint(0, 255), randint(0, 255)))
    return img

def add_detected(img, res):
    if res.shape[-1] > 0:
        for i in range(res.shape[-1]):
            color = (randint(0, 255), randint(0, 255), randint(0, 255))
            for x in range(res.shape[0]):
                for y in range(res.shape[1]):
                    if res[x][y][i] > 0:
                        img[x][y] = color
    return img

def train(model):
    """Train the model."""
    # Training dataset.
    dataset_train = BarrelDataset()
    dataset_train.load_barrel(args.dataset, "train")
    dataset_train.prepare()

    # Validation dataset
    dataset_val = BarrelDataset()
    dataset_val.load_barrel(args.dataset, "val")
    dataset_val.prepare()

    # Augmentation that flips the image (from left to right half the time)
    augmentation = imgaug.augmenters.Fliplr(0.5)

    # *** This training schedule is an example. Update to your needs ***
    # First train just the heads (RPN, mask and classifier)
    # Then train layers from resnet101 stage 4
    # Then train all layers but very carefully, with low learning rate
    print("Training network heads")
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=30,
                layers='heads',
                augmentation=augmentation)
    
    print("Fine tune Resnet stage 4 and up")
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=500,
                layers='4+',
                augmentation=augmentation)

    print("Fine tune all layers")
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE / 10,
                epochs=1000,
                layers='all',
                augmentation=augmentation)
    

############################################################
#  Training
############################################################

if __name__ == '__main__':
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Train Mask R-CNN on barrel dataset.')
    parser.add_argument("command",
                        metavar="<command>",
                        help="'train' or 'eval'")
    parser.add_argument('--dataset', required=True,
                        metavar="/path/to/coco/",
                        help='Directory of the MS-COCO dataset')
    parser.add_argument('--model', required=True,
                        metavar="/path/to/weights.h5",
                        help="Path to weights .h5 file or 'coco'")
    parser.add_argument('--logs', required=False,
                        default=DEFAULT_LOGS_DIR,
                        metavar="/path/to/logs/",
                        help='Logs and checkpoints directory (default=logs/)')
    parser.add_argument('--image', required=False,
                        metavar="path or URL to image",
                        help='Image to evaluate on')
    parser.add_argument('--video', required=False,
                        metavar="path or URL to video",
                        help='Video to evaluate on')
    parser.add_argument('--capture', required=False,
                        metavar="0",
                        help="Camera id for videoCapture")
    parser.add_argument('--folder', required=False,
                        metavar="path/to/folder",
                        help="Folder to read imgs from")
    parser.add_argument('--verbose', required=False,
                        metavar=False,
                        help="Be verbose when visualizing eval")
    parser.add_argument('--runConstantly', required=False,
                        metavar=False,
                        help="Be verbose when visualizing eval")
    
    args = parser.parse_args()

    #sys.stdout = Logger(args.logs + "/log" + str(randint(1, 999)) + ".txt")
    # Check if a file with chosen random name already exists
    while True:
        logsuffix = randint(1, 999)
        if not os.path.isfile(args.logs + "/log" + str(randint(1, 999)) + ".txt"):
            break
    #sys.stdout = Logger(args.logs + "/log" + str(randint(1, 999)) + ".txt")

    # Validate arguments
    if args.command == "train":
        assert args.dataset, "Argument --dataset is required for training"
    elif args.command == "eval":
        assert args.image or args.video or args.capture or args.folder,\
               "Provide --image or --video or --capture or --folder to evaluate networks' performance"

    print("Dataset: ", args.dataset)
    print("Logs: ", args.logs)
    print("Model: ", args.model)

    # Configurations
    if args.command == "train":
        config = BarrelConfig()
    else:
        class InferenceConfig(BarrelConfig):
            # Set batch size to 1 since we'll be running inference on
            # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
            GPU_COUNT = 1
            IMAGES_PER_GPU = 1
            DETECTION_MIN_CONFIDENCE = confidence_border

        config = InferenceConfig()
    config.display()

    # Create model
    if args.command == "train":
        model = modellib.MaskRCNN(mode="training", config=config,
                                  model_dir=args.logs)
    else:
        model = modellib.MaskRCNN(mode="inference", config=config,
                                  model_dir=args.logs)

    # Select weights file to load
    if args.model.lower() == "coco":
        weights_path = COCO_WEIGHTS_PATH
        # Download weights file
        if not os.path.exists(weights_path):
            utils.download_trained_weights(weights_path)
    elif args.model.lower() == "last":
        # Find last trained weights
        weights_path = model.find_last()
    elif args.model.lower() == "imagenet":
        # Start from ImageNet trained weights
        weights_path = model.get_imagenet_weights()
    else:
        weights_path = args.model

    # Load weights
    print("Loading weights ", weights_path)
    if args.model.lower() == "coco":
        # Exclude the last layers because they require a matching
        # number of classes
        model.load_weights(weights_path, by_name=True, exclude=[
            "mrcnn_class_logits", "mrcnn_bbox_fc",
            "mrcnn_bbox", "mrcnn_mask"])
    else:
        model.load_weights(weights_path, by_name=True)

    # Train or evaluate
    if args.command == "train":
        print("Starting training")
        train(model)
    elif args.command == "eval":
        if args.image:
            eval_model(model, image_path=args.image)
        elif args.video:
            eval_model(model, video_path=args.video)
        elif args.folder:
            if args.verbose:
                eval_model(model, folder=args.folder, showimg=False, verbose=True, runConstantly=args.runConstantly)
            else:
                eval_model(model, folder=args.folder, showimg=False, verbose=False, runConstantly=args.runConstantly)
        elif args.capture:
            cap = cv2.VideoCapture(int(args.capture))
            eval_model(model, videoCapture=cap)
    else:
        print("'{}' is not recognized. "
              "Use 'train' or 'eval'".format(args.command))
