'''
test for jsons and their parsing
loads all jsons in jsonpath and displays the pics
with barrels selected
'''

import json
from cv2 import cv2
from random import randint
import os
import sys

jsonpath = sys.argv[1]

colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]

for jsonfilename in os.listdir(jsonpath):
    print('opening json ' + jsonfilename)
    newJson = json.load(open(jsonpath + jsonfilename))
    img = cv2.imread(jsonpath + '../pics/' + newJson["filename"])

    barrelsNum = newJson["amount"]

    for i in range(barrelsNum):
        x = newJson["barrels"][str(i)]["y"]
        y = newJson["barrels"][str(i)]["x"]

        for j in range(len(x)):
            img[x[j]][y[j]] = colors[newJson['barrels'][str(i)]["type"]+1]

    for i in range(barrelsNum):
        try:
            barrelType = newJson['barrels'][str(i)]["type"]
            if barrelType == 0:
                barrelType = 1
            elif barrelType == 1:
                barrelType = 2
            else:
                raise Exception("barrel type must be standing or lying")
        except KeyError:
            barrelType = ''

        x = newJson["barrels"][str(i)]["y"]
        y = newJson["barrels"][str(i)]["x"]

        cv2.putText(img, str(i), (y[-1], x[-1]),  cv2.FONT_HERSHEY_SCRIPT_SIMPLEX, 0.5, colors[0])
    
    cv2.imwrite(newJson['filename'], img)